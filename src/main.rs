use std::process::exit;

use actix_service::Service;
use actix_web::{
    http::{self, HeaderName, HeaderValue},
    App, HttpServer,
};

mod api;
mod config;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config = match crate::config::FerrixConfig::new("./config.toml") {
        Ok(config) => config,
        Err(why) => {
            println!("Unable to open config: {}", why);
            exit(1);
        }
    };

    let config_copy = config.clone();
    let mut server = HttpServer::new(move || {
        App::new()
            .data(config_copy.clone())
            .service(crate::api::clientserver::get_scope().wrap_fn(|req, srv| {
                let is_options = *req.method() == http::Method::OPTIONS;
                let fut = srv.call(req);
                async move {
                    let mut res = fut.await?;
                    if is_options {
                        let headers = res.headers_mut();
                        headers.insert(
                            HeaderName::from_bytes(b"access-control-allow-origin").unwrap(),
                            HeaderValue::from_static("*"),
                        );
                        headers.insert(
                            HeaderName::from_bytes(b"access-control-allow-methods").unwrap(),
                            HeaderValue::from_static("GET, POST, PUT, DELETE, OPTIONS"),
                        );
                        headers.insert(
                            HeaderName::from_bytes(b"access-control-allow-headers").unwrap(),
                            HeaderValue::from_static(
                                "Origin, X-Requested-With, Content-Type, Accept, Authorization",
                            ),
                        );
                    }
                    Ok(res)
                }
            }))
    });

    for addr in &config.bind {
        server = match server.bind(addr) {
            Ok(server) => server,
            Err(why) => {
                println!("Failed to bind to {}: {}", addr, why);
                exit(1);
            }
        };
        println!("Bound to {}", addr);
    }

    println!("Starting server");
    server.run().await
}
