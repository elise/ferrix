use config::{ConfigError, File};
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct JwtKeys {
    pub(crate) public: String,
    pub(crate) private: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct FerrixConfig {
    pub(crate) bind: Vec<std::net::SocketAddr>,
    pub(crate) homeserver_hostname: String,
    pub(crate) identity_server_hostname: String,
    pub(crate) jwt_keys: JwtKeys,
}

impl FerrixConfig {
    pub fn new(config_file_path: &str) -> Result<Self, ConfigError> {
        let mut config = config::Config::new();
        config.merge(File::with_name(config_file_path))?;
        config.try_into()
    }
}
