use actix_web::{web, Scope};

pub mod auth;
pub mod error;
pub mod responses;
pub mod standards;

fn get_matrix_scope() -> Scope {
    web::scope("/_matrix/client").service(standards::versions)
}

pub fn get_scope() -> Scope {
    web::scope("")
        .service(get_matrix_scope())
        .service(standards::well_known_url)
}
