use actix_web::{route, web, HttpResponse};

use crate::config::FerrixConfig;

use super::responses::{
    create_ok_json_http_response, ServerBaseUrlInformation, VersionsResponse, WellKnownUrlResponse,
};

const SUPPORTED_VERSIONS: [&'static str; 1] = ["r0.6.1"];

#[route("/versions", method = "GET", method = "OPTIONS")]
pub async fn versions() -> HttpResponse {
    create_ok_json_http_response(VersionsResponse {
        versions: vec![
            SUPPORTED_VERSIONS[0].to_string(),
            SUPPORTED_VERSIONS[SUPPORTED_VERSIONS.len() - 1].to_string(),
        ],
        unstable_features: None,
    })
}

#[route("/.well-known/matrix/client", method = "GET", method = "OPTIONS")]
pub async fn well_known_url(config: web::Data<FerrixConfig>) -> HttpResponse {
    create_ok_json_http_response(WellKnownUrlResponse {
        homeserver: ServerBaseUrlInformation {
            base_url: config.homeserver_hostname.clone(),
        },
        identity_server: ServerBaseUrlInformation {
            base_url: config.identity_server_hostname.clone(),
        },
    })
}
