use std::str::FromStr;

use serde::{de::Visitor, Deserialize, Serialize};

#[derive(Debug)]
pub enum AuthenticationType {
    Password,
    Recaptcha,
    OAuth2,
    SSO,
    EmailIdentityServer,
    PhoneNumberIdentityServer,
    Token,
    Dummy,
}

#[derive(Debug)]
pub enum IdentifierType {
    MatrixId,
    ThirdPartyId,
    PhoneNumber,
}

impl FromStr for AuthenticationType {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "m.login.password" => Ok(AuthenticationType::Password),
            "m.login.recaptcha" => Ok(AuthenticationType::Recaptcha),
            "m.login.oauth2" => Ok(AuthenticationType::OAuth2),
            "m.login.sso" => Ok(AuthenticationType::SSO),
            "m.login.email.identity" => Ok(AuthenticationType::EmailIdentityServer),
            "m.login.msisdn" => Ok(AuthenticationType::PhoneNumberIdentityServer),
            "m.login.token" => Ok(AuthenticationType::Token),
            "m.login.dummy" => Ok(AuthenticationType::Dummy),
            _ => Err("Invalid Authentication Type"),
        }
    }
}

impl ToString for AuthenticationType {
    fn to_string(&self) -> String {
        String::from(match self {
            AuthenticationType::Password => "m.login.password",
            AuthenticationType::Recaptcha => "m.login.recaptcha",
            AuthenticationType::OAuth2 => "m.login.oauth2",
            AuthenticationType::SSO => "m.login.sso",
            AuthenticationType::EmailIdentityServer => "m.login.email.identity",
            AuthenticationType::PhoneNumberIdentityServer => "m.login.msisdn",
            AuthenticationType::Token => "m.login.token",
            AuthenticationType::Dummy => "m.login.dummy",
        })
    }
}

struct AuthenticationTypeVisitor;

impl<'de> Visitor<'de> for AuthenticationTypeVisitor {
    type Value = AuthenticationType;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an Authentication Type")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::Value::from_str(v).unwrap())
    }
}

impl<'de> Deserialize<'de> for AuthenticationType {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(AuthenticationTypeVisitor)
    }
}

impl Serialize for AuthenticationType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}

impl FromStr for IdentifierType {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "m.id.user" => Ok(IdentifierType::MatrixId),
            "m.id.thirdparty" => Ok(IdentifierType::ThirdPartyId),
            "m.id.phone" => Ok(IdentifierType::PhoneNumber),
            _ => Err("Invalid Identification Type"),
        }
    }
}

impl ToString for IdentifierType {
    fn to_string(&self) -> String {
        String::from(match self {
            IdentifierType::MatrixId => "m.id.user",
            IdentifierType::ThirdPartyId => "m.id.thirdparty",
            IdentifierType::PhoneNumber => "m.id.phone",
        })
    }
}

struct IdentifierTypeVisitor;

impl<'de> Visitor<'de> for IdentifierTypeVisitor {
    type Value = IdentifierType;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("an Identifier Type")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::Value::from_str(v).unwrap())
    }
}

impl<'de> Deserialize<'de> for IdentifierType {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(IdentifierTypeVisitor)
    }
}

impl Serialize for IdentifierType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}
