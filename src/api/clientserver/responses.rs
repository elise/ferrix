use actix_web::HttpResponse;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

use super::error::ErrorCode;

pub fn create_ok_json_http_response<T: Serialize>(body: T) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("application/json")
        .json(body)
}

#[derive(Deserialize, Serialize, Debug)]
pub struct StandardErrorResponse {
    pub(crate) errcode: ErrorCode,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) error: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct VersionsResponse {
    pub(crate) versions: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) unstable_features: Option<Map<String, Value>>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ServerBaseUrlInformation {
    pub(crate) base_url: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct WellKnownUrlResponse {
    #[serde(rename = "m.homeserver")]
    pub(crate) homeserver: ServerBaseUrlInformation,
    #[serde(rename = "m.identity_server")]
    pub(crate) identity_server: ServerBaseUrlInformation,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct LoginStagesInformation {
    pub(crate) stages: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct AuthenticationFlowResponse {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) errcode: Option<ErrorCode>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) error: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) completed: Option<Vec<String>>,
    pub(crate) flows: Vec<LoginStagesInformation>,
    pub(crate) params: Map<String, Value>,
    pub(crate) session: String,
    pub(crate) soft_logout: bool,
}
