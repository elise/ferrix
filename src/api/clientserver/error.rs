use std::{fmt::Display, str::FromStr};

use actix_web::{HttpResponse, ResponseError};
use serde::{de::Visitor, Deserialize, Serialize};

use super::responses::{AuthenticationFlowResponse, StandardErrorResponse};

#[derive(Debug)]
pub enum ErrorCode {
    /// Forbidden access, e.g. joining a room without permission, failed login.
    MatrixForbidden,
    /// The access token specified was not recognised
    MatrixUnknownToken,
    /// No access token was specified for the request
    MatrixMissingToken,
    /// Request contained valid JSON, but it was malformed in some way
    MatrixBadJson,
    /// Request did not contain valid JSON
    MatrixNotJson,
    /// No resource was found for this request
    MatrixNotFound,
    /// Too many requests have been sent in a short period of time. Wait a while then try again
    MatrixLimitExceeded,
    /// An unknown error has occurred
    MatrixUnknown,
    /// The server did not understand the request
    MatrixUnrecognized,
    /// The request was not correctly authorized. Usually due to login failures
    MatrixUnauthorized,
    /// The user ID associated with the request has been deactivated
    MatrixUserDeactivated,
    /// Encountered when trying to register a user ID which has been taken
    MatrixUserInUse,
    /// Encountered when trying to register a user ID which is not valid
    MatrixInvalidUsername,
    /// Sent when the room alias given to the `createRoom` API is already in use
    MatrixRoomInUse,
    /// Sent when the initial state given to the `createRoom` API is invalid
    MatrixInvalidRoomState,
    /// Sent when a threepid given to an API cannot be used because the same threepid is already in use
    MatrixThreepidInUse,
    /// Sent when a threepid given to an API cannot be used because no record matching the threepid was found
    MatrixThreepidNotFound,
    /// Authentication could not be performed on the third party identifier
    MatrixThreepidAuthFailed,
    /// The server does not permit this third party identifier
    MatrixThreepidDenied,
    /// The client's request used a third party server, eg. identity server, that this server does not trust
    MatrixServerNotTrusted,
    /// The client's request to create a room used a room version that the server does not support
    MatrixUnsupportedRoomVersion,
    /// The client attempted to join a room that has a version the server does not support. Inspect the `room_version` property of the error response for the room's version.
    MatrixIncompatibleRoomVersion,
    /// The state change requested cannot be performed
    MatrixBadState,
    /// The room or resource does not permit guests to access it
    MatrixGuestAccessForbidden,
    /// A Captcha is required to complete the request
    MatrixCaptchaNeeded,
    /// The Captcha provided did not match what was expected
    MatrixCaptchaInvalid,
    /// A required parameter was missing from the request
    MatrixMissingParam,
    /// A parameter that was specified has the wrong value
    MatrixInvalidParam,
    /// The request or entity was too large
    MatrixTooLarge,
    /// The resource being requested is reserved by an application service, or the application service making the request has not created the resource
    MatrixExclusive,
    /// The request cannot be completed because the homeserver has reached a resource limit imposed on it
    MatrixResourceLimitExceeded,
    /// The user is unable to reject an invite to join the server notices room
    MatrixCannotLeaveServerNoticeRoom,
    /// Other Error, String is value
    Other(String),
}

impl FromStr for ErrorCode {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "M_FORBIDDEN" => ErrorCode::MatrixForbidden,
            "M_UNKNOWN_TOKEN" => ErrorCode::MatrixMissingToken,
            "M_MISSING_TOKEN" => ErrorCode::MatrixUnknownToken,
            "M_BAD_JSON" => ErrorCode::MatrixBadJson,
            "M_NOT_JSON" => ErrorCode::MatrixNotJson,
            "M_NOT_FOUND" => ErrorCode::MatrixNotFound,
            "M_LIMIT_EXCEEDED" => ErrorCode::MatrixLimitExceeded,
            "M_UNKNOWN" => ErrorCode::MatrixUnknown,
            "M_UNRECOGNIZED" => ErrorCode::MatrixUnrecognized,
            "M_UNAUTHORIZED" => ErrorCode::MatrixUnauthorized,
            "M_USER_DEACTIVATED" => ErrorCode::MatrixUserDeactivated,
            "M_USER_IN_USE" => ErrorCode::MatrixUserInUse,
            "M_INVALID_USERNAME" => ErrorCode::MatrixInvalidUsername,
            "M_ROOM_IN_USE" => ErrorCode::MatrixRoomInUse,
            "M_INVALID_ROOM_STATE" => ErrorCode::MatrixInvalidRoomState,
            "M_THREEPID_IN_USE" => ErrorCode::MatrixThreepidInUse,
            "M_THREEPID_NOT_FOUND" => ErrorCode::MatrixThreepidNotFound,
            "M_THREEPID_AUTH_FAILED" => ErrorCode::MatrixThreepidAuthFailed,
            "M_THREEPID_DENIED" => ErrorCode::MatrixThreepidDenied,
            "M_SERVER_NOT_TRUSTED" => ErrorCode::MatrixServerNotTrusted,
            "M_UNSUPPORTED_ROOM_VERSION" => ErrorCode::MatrixUnsupportedRoomVersion,
            "M_INCOMPATIBLE_ROOM_VERSION" => ErrorCode::MatrixIncompatibleRoomVersion,
            "M_BAD_STATE" => ErrorCode::MatrixBadState,
            "M_GUEST_ACCESS_FORBIDDEN" => ErrorCode::MatrixGuestAccessForbidden,
            "M_CAPTCHA_NEEDED" => ErrorCode::MatrixCaptchaNeeded,
            "M_CAPTCHA_INVALID" => ErrorCode::MatrixCaptchaInvalid,
            "M_MISSING_PARAM" => ErrorCode::MatrixMissingParam,
            "M_INVALID_PARAM" => ErrorCode::MatrixInvalidParam,
            "M_TOO_LARGE" => ErrorCode::MatrixTooLarge,
            "M_EXCLUSIVE" => ErrorCode::MatrixExclusive,
            "M_RESOURCE_LIMIT_EXCEEDED" => ErrorCode::MatrixResourceLimitExceeded,
            "M_CANNOT_LEAVE_SERVER_NOTICE_ROOM" => ErrorCode::MatrixCannotLeaveServerNoticeRoom,
            val => ErrorCode::Other(val.to_string()),
        })
    }
}

struct ErrorCodeVisitor;

impl<'de> Visitor<'de> for ErrorCodeVisitor {
    type Value = ErrorCode;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a Matrix Error Code")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::Value::from_str(v).unwrap())
    }
}

impl<'de> Deserialize<'de> for ErrorCode {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_string(ErrorCodeVisitor)
    }
}

impl Serialize for ErrorCode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}

impl Display for ErrorCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            ErrorCode::MatrixForbidden => "M_FORBIDDEN",
            ErrorCode::MatrixMissingToken => "M_UNKNOWN_TOKEN",
            ErrorCode::MatrixUnknownToken => "M_MISSING_TOKEN",
            ErrorCode::MatrixBadJson => "M_BAD_JSON",
            ErrorCode::MatrixNotJson => "M_NOT_JSON",
            ErrorCode::MatrixNotFound => "M_NOT_FOUND",
            ErrorCode::MatrixLimitExceeded => "M_LIMIT_EXCEEDED",
            ErrorCode::MatrixUnknown => "M_UNKNOWN",
            ErrorCode::MatrixUnrecognized => "M_UNRECOGNIZED",
            ErrorCode::MatrixUnauthorized => "M_UNAUTHORIZED",
            ErrorCode::MatrixUserDeactivated => "M_USER_DEACTIVATED",
            ErrorCode::MatrixUserInUse => "M_USER_IN_USE",
            ErrorCode::MatrixInvalidUsername => "M_INVALID_USERNAME",
            ErrorCode::MatrixRoomInUse => "M_ROOM_IN_USE",
            ErrorCode::MatrixInvalidRoomState => "M_INVALID_ROOM_STATE",
            ErrorCode::MatrixThreepidInUse => "M_THREEPID_IN_USE",
            ErrorCode::MatrixThreepidNotFound => "M_THREEPID_NOT_FOUND",
            ErrorCode::MatrixThreepidAuthFailed => "M_THREEPID_AUTH_FAILED",
            ErrorCode::MatrixThreepidDenied => "M_THREEPID_DENIED",
            ErrorCode::MatrixServerNotTrusted => "M_SERVER_NOT_TRUSTED",
            ErrorCode::MatrixUnsupportedRoomVersion => "M_UNSUPPORTED_ROOM_VERSION",
            ErrorCode::MatrixIncompatibleRoomVersion => "M_INCOMPATIBLE_ROOM_VERSION",
            ErrorCode::MatrixBadState => "M_BAD_STATE",
            ErrorCode::MatrixGuestAccessForbidden => "M_GUEST_ACCESS_FORBIDDEN",
            ErrorCode::MatrixCaptchaNeeded => "M_CAPTCHA_NEEDED",
            ErrorCode::MatrixCaptchaInvalid => "M_CAPTCHA_INVALID",
            ErrorCode::MatrixMissingParam => "M_MISSING_PARAM",
            ErrorCode::MatrixInvalidParam => "M_INVALID_PARAM",
            ErrorCode::MatrixTooLarge => "M_TOO_LARGE",
            ErrorCode::MatrixExclusive => "M_EXCLUSIVE",
            ErrorCode::MatrixResourceLimitExceeded => "M_RESOURCE_LIMIT_EXCEEDED",
            ErrorCode::MatrixCannotLeaveServerNoticeRoom => "M_CANNOT_LEAVE_SERVER_NOTICE_ROOM",
            ErrorCode::Other(other) => other.as_str(),
        })
    }
}

impl Display for StandardErrorResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(serde_json::to_string(self).unwrap().as_str())
    }
}

impl ResponseError for StandardErrorResponse {
    fn error_response(&self) -> actix_web::HttpResponse {
        match self.errcode {
            ErrorCode::MatrixCaptchaNeeded
            | ErrorCode::MatrixCaptchaInvalid
            | ErrorCode::MatrixThreepidDenied
            | ErrorCode::MatrixThreepidAuthFailed
            | ErrorCode::MatrixUserDeactivated
            | ErrorCode::MatrixForbidden => HttpResponse::Forbidden().json(self),
            ErrorCode::MatrixGuestAccessForbidden
            | ErrorCode::MatrixUnauthorized
            | ErrorCode::MatrixUnknownToken
            | ErrorCode::MatrixMissingToken => HttpResponse::Unauthorized().json(self),
            ErrorCode::Other(_)
            | ErrorCode::MatrixCannotLeaveServerNoticeRoom
            | ErrorCode::MatrixExclusive
            | ErrorCode::MatrixMissingParam
            | ErrorCode::MatrixInvalidParam
            | ErrorCode::MatrixUnsupportedRoomVersion
            | ErrorCode::MatrixIncompatibleRoomVersion
            | ErrorCode::MatrixBadState
            | ErrorCode::MatrixServerNotTrusted
            | ErrorCode::MatrixThreepidInUse
            | ErrorCode::MatrixThreepidNotFound
            | ErrorCode::MatrixRoomInUse
            | ErrorCode::MatrixInvalidRoomState
            | ErrorCode::MatrixUserInUse
            | ErrorCode::MatrixInvalidUsername
            | ErrorCode::MatrixUnknown
            | ErrorCode::MatrixBadJson
            | ErrorCode::MatrixNotJson => HttpResponse::BadRequest().json(self),
            ErrorCode::MatrixUnrecognized | ErrorCode::MatrixNotFound => {
                HttpResponse::NotFound().json(self)
            }
            ErrorCode::MatrixResourceLimitExceeded | ErrorCode::MatrixLimitExceeded => {
                HttpResponse::ServiceUnavailable().json(self)
            }
            ErrorCode::MatrixTooLarge => HttpResponse::PayloadTooLarge().json(self),
        }
    }
}

impl Display for AuthenticationFlowResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(serde_json::to_string(self).unwrap().as_str())
    }
}

impl ResponseError for AuthenticationFlowResponse {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::Unauthorized().json(self)
    }
}
