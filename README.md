# Ferrix

A toy Matrix homeserver written in Rust, for me to learn more about the Matrix protocol

## License

The source is licened under [MIT](https://gitlab.com/elise/ferrix/-/blob/main/LICENSE.txt)